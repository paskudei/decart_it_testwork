Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  resources :currencies, only: [:index, :show], defaults: { format: :json }
end
