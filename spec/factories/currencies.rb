FactoryBot.define do
  factory :currency do
    name { Faker::Currency.name }
    rate { Faker::Number.decimal(l_digits: 2) }
  end
end
