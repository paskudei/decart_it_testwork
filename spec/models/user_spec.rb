RSpec.describe User, type: :model do
  it { expect(build(:user)).to be_valid }

  let!(:user) { create(:user) }

  describe 'ActiveModel' do
    context 'validations' do
      it { expect(user).to validate_presence_of(:email) }
      it { expect(user).to validate_uniqueness_of(:email) }
      it { expect(user).to validate_presence_of(:password_digest) }
    end
  end
end
