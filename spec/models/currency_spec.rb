RSpec.describe Currency, type: :model do
  it { expect(build(:currency)).to be_valid }

  let!(:currency) { create(:currency) }

  describe 'ActiveModel' do
    context 'validations' do
      it { expect(currency).to validate_presence_of(:name) }
      it { expect(currency).to validate_uniqueness_of(:name) }
      it { expect(currency).to validate_presence_of(:rate) }
    end
  end
end
