RSpec.describe 'Currencies', type: :request do
  let!(:currency) { create(:currency) }
  let!(:user) { create(:user) }

  describe 'GET #index' do
    context 'with auth token' do
      before(:each) { get '/currencies', headers: auth_headers(user), as: :json }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template('index') }
      it { expect(response).to render_template('_currency') }
      it { expect(response).to match_response_schema('currencies') }
    end

    context 'without auth token' do
      before(:each) { get '/currencies' }

      it { expect(response).to have_http_status(:unauthorized) }
    end
  end
end
