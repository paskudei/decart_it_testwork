namespace :synch do
  task currencies: :environment do
    currencies.each { |currency| Currency.find_or_create_by(name: currency[:name]).update(rate: currency[:value]) }
  end

  def request
    Savon.client(
      endpoint: 'http://www.cbr.ru/scripts/XML_daily.asp',
      namespace: 'http://web.cbr.ru/'
    )
  end

  def response
    request.call(:get_curs_on_date)
  end

  def currencies
    response.hash[:val_curs][:valute]
  end
end
