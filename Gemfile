source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

gem 'aasm'
gem 'awesome_print'
gem 'bcrypt', '~> 3.1.7'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'faker'
gem 'jbuilder', '~> 2.5'
gem 'jwt'
gem 'kaminari'
gem 'knock'
gem 'listen', '>= 3.0.5', '< 3.2'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'rack-cors'
gem 'rails', '~> 5.2.0'
gem 'savon'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

group :development do
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '>= 2.15'
end

group :test, :ci do
  gem 'database_cleaner'
  gem 'fuubar'
  gem 'json_matchers', '~> 0.9.0'
  gem 'rails-controller-testing'
  gem 'shoulda-callback-matchers'
  gem 'shoulda-matchers'
end

group :development, :test, :ci do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'pry'
  gem 'rspec-rails'
  gem 'rubocop-performance'
end
