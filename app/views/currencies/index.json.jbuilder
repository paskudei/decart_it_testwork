json.total @total
json.data @currencies do |currency|
  json.partial! partial: 'currencies/currency', locals: { currency: currency }
end
