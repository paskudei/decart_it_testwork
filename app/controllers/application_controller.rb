class ApplicationController < ActionController::Base
  include Knock::Authenticable
  include Paginated
end
