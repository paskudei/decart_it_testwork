class CurrenciesController < ApplicationController
  before_action :_set_currency, only: [:show]
  before_action :authenticate_user, only: [:index, :show]

  def index
    @currencies = apply_pagination(Currency)
    @total = @currencies.total_count
  end

  def show; end

  private

  def _set_currency
    @currency = Currency.find(params[:id])
  end
end
