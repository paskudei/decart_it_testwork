module Paginated
  extend ActiveSupport::Concern

  included do
    def apply_pagination(resource)
      resource.page(current_page).per(resource_count)
    end

    def current_page
      params[:page].to_i
    end

    def resource_count
      params[:per_page] || _default_count
    end
  end

  private

  def _default_count
    25
  end
end
