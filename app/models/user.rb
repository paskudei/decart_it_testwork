class User < ApplicationRecord
  include Authenticatable

  # Attributes
  has_secure_password
  attr_accessor :token

  # Validations
  validates :email, presence: true, uniqueness: true
  validates :password_digest, presence: true
end
