class Currency < ApplicationRecord
  # Validations
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :rate, presence: true
end
